package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http/httputil"
	"net/url"
	"os"
)

func main() {
	// initialize Gin webserver
	api := gin.Default()

	// setup API endpoint for registration tokens
	api.POST("/api/ldap-helper", requestRegistrationToken)

	// reverse-proxy all other requests to VAULTWARDEN_BACKEND
	vaultwardenBackendUrl, err := url.Parse(os.Getenv("VAULTWARDEN_BACKEND"))
	if err != nil {
		panic(fmt.Errorf("can't parse VAULTWARDEN_BACKEND as an URL: %w", err))
	}
	vaultwardenBackendProxy := httputil.NewSingleHostReverseProxy(vaultwardenBackendUrl)
	api.Any("/*path", gin.WrapH(vaultwardenBackendProxy))

	// start the server
	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
	}
	panic(api.Run("[::]:" + port))
}

// requestRegistrationToken checks LDAP credentials & then creates and returns a registration token
func requestRegistrationToken(c *gin.Context) {
	// TODO
}

// checkCredentials checks username & password via LDAP
func checkCredentials(username, password string) error {
	// TODO
	return nil
}

// createRegistrationToken adds a token in the Vaultwarden database
func createRegistrationToken(email, name string) (string, error) {
	// TODO
	return "", nil
}

// TODO: think of a concept for email changes in Peopled!
