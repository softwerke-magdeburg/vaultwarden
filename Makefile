TAG ?= alpine
.PHONY: build build-web build-helper apply-patchfile update-patchfile

build: build-web build-ldap-helper
build-ldap-helper:
	docker build --pull \
	  -t "codeberg.org/softwerke-magdeburg/vaultwarden:ldap-helper" \
	  ./ldap-helper
build-web:
	# Build patched web vault
	make apply-patchfile
	rsync -a frontend-registration-flow/ bw_web_builds/frontend-registration-flow
	docker build --pull \
	  -t "codeberg.org/softwerke-magdeburg/vaultwarden:web" \
	  -f frontend-registration-flow/Dockerfile \
	  ./bw_web_builds
	cd bw_web_builds && git add -A && git reset --hard
	cd bw_web_builds && git add -A && git reset --hard # Twice to also reset .gitignore'd files
	# Build patched vaultwarden container
	docker pull "vaultwarden/server:$(TAG)"
	docker build \
	  -t "codeberg.org/softwerke-magdeburg/vaultwarden:$(TAG)" \
	  --build-arg "TAG=$(TAG)" \
	  ./patched-vaultwarden

update:
	cd bw_web_builds && git add -A && git reset --hard \
	 && git fetch --tags \
	 && git checkout $$(git tag --list --sort committerdate | tail -n1)
	make build
	# TODO: commit update

apply-patchfile:
	cd bw_web_builds && git add -A && git reset --hard
	cp bw_web_builds/Dockerfile frontend-registration-flow/Dockerfile
	patch frontend-registration-flow/Dockerfile < frontend-registration-flow/Dockerfile.patch
update-patchfile:
	cd bw_web_builds && git add -A && git reset --hard
	diff -U 2 bw_web_builds/Dockerfile frontend-registration-flow/Dockerfile > frontend-registration-flow/Dockerfile.patch; \
	  [ $$? -le 1 -a $$? -ge 0 ] # exit code 1 means "Differences were found.", which is intended behaviour and thus okay.
