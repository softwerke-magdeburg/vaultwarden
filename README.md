# Vaultwarden Distribution by Softwerke Magdeburg

This repository contains our patched distribution of [Vaultwarden](https://github.com/dani-garcia/vaultwarden) with a custom registration page for a more straightforward LDAP registration flow.
